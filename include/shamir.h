#ifndef SHAMIR_H_
#define SHAMIR_H_
#include <cryptopp/ida.h>
#include <cryptopp/osrng.h>
#include <vector>
#include <string>
#include <iostream>

//split secret string to n shards
std::vector<std::string> SecretShare(size_t threshold, size_t nShares, std::string secret); 


//recover t strings to secret
std::string SecretRecover(size_t threshold, std::vector<std::string> input_str); 
#endif // SHAMIR_H_
