#ifndef HEX_FUNCTION_H_
#define HEX_FUNCTION_H_
#include <string>
#include <vector>

#include <cryptopp/hex.h>

std::string unhex_str(std::string input);  // convert str hex to str bytes
std::string hex_str(std::string input) ;   // convert str bytes to str hex
std::vector<std::string> hex(std::vector<std::string> input) ;  // bytes->hex
std::vector<std::string> encode_hex(std::vector<std::string> input) ;  // hex->bytes
#endif //HEX_FUNCTIONS_H_
