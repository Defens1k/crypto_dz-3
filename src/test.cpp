#include <string>
#include "rsa_functions.h"

using namespace CryptoPP;
using namespace std;

void SecretRecoverFile(int threshold, string & out, vector<string> input_str)
{
    SecretRecovery recovery(threshold, new StringSink(out));

    vector_member_ptrs<StringSource> fileSources(threshold);
    SecByteBlock channel(4);
    int i;
    for (i=0; i<threshold; i++)
    {
        fileSources[i].reset(new StringSource(input_str[i], false));
        fileSources[i]->Pump(4);
        fileSources[i]->Get(channel, 4);
        fileSources[i]->Attach(new ChannelSwitch(recovery, string((char *)channel.begin(), 4)));
    }

    while (fileSources[0]->Pump(256))
        for (i=1; i<threshold; i++)
            fileSources[i]->Pump(256);

    for (i=0; i<threshold; i++)
        fileSources[i]->PumpAll();
}

vector<string> SecretShareFile(int threshold, int nShares, string secret)
{
    AutoSeededRandomPool rng;

    ChannelSwitch *channelSwitch;
    StringSource source(secret, false, new SecretSharing(rng,
        threshold, nShares, channelSwitch = new ChannelSwitch));
    vector<string> out_str;
    vector_member_ptrs<StringSink> fileSinks(nShares);
    string channel;
    for (int i = 0; i < nShares; i++) {
        out_str.push_back(string());
    }
    for (int i=0; i<nShares; i++)
    {
        char extension[5] = ".000";
        extension[1]='0'+CryptoPP::byte(i/100);
        extension[2]='0'+CryptoPP::byte((i/10)%10);
        extension[3]='0'+CryptoPP::byte(i%10);
        fileSinks[i].reset(new StringSink(out_str[i]));

        channel = WordToString<word32>(i);
        fileSinks[i]->Put((CryptoPP::byte *)channel.data(), 4);
        channelSwitch->AddRoute(channel, *fileSinks[i], DEFAULT_CHANNEL);
    }

    source.PumpAll();
    return out_str;
}

string unhex_str(string input) {
    string out;
       
    StringSource ss(input, true,
        new HexDecoder(
            new StringSink(out)
        ) // HexDecoder
    ); // StringSource
    return out;
}

string hex_str(string input) {
    string out;

    StringSource ss(input, true,
        new HexEncoder(
            new StringSink(out)
        ) // HexEncoder
    ); // StringSource

    return out;
}

vector<string> hex(vector<string> input) {
    vector<string> out;
    for (auto str : input) {
        out.push_back(hex_str(str));
    }
    return out;
}

vector<string> encode_hex(vector<string> input) {
    vector<string> out;
    for (auto str : input) {
        out.push_back(unhex_str(str));
    }
    return out;
}

int main() {
    vector<string> v = SecretShareFile(2, 6, string("this is secret!"));
    vector<string> v_hexed = hex(v);
    for (auto s : v_hexed) {
        std::cout << s << std::endl;
    }
    v = encode_hex(v_hexed);
    string out;
    SecretRecoverFile(2, out, v);
    std::cout << out << std::endl;
}


