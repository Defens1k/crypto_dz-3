#include "rsa_functions.h"

CryptoPP::SecByteBlock str_to_block(const std::string & s) {
    return CryptoPP::SecByteBlock(reinterpret_cast<const CryptoPP::byte*>(s.data()), s.size());
}

std::string block_to_str(const CryptoPP::SecByteBlock & block) {
    return std::string(reinterpret_cast<const char*>(block.data()), block.size());
}





void Save(const std::string& filename, const CryptoPP::BufferedTransformation& bt) {
    CryptoPP::FileSink file(filename.c_str());

    bt.CopyTo(file);
    file.MessageEnd();
}

void SavePublicKey(const std::string& filename, const CryptoPP::RSA::PublicKey& key) {
    CryptoPP::ByteQueue queue;
    key.Save(queue);

    Save(filename, queue);
}

void SavePrivateKey(const std::string& filename, const CryptoPP::RSA::PrivateKey& key) {
    CryptoPP::ByteQueue queue;
    key.Save(queue);

    Save(filename, queue);
}

void Load(const std::string& filename, CryptoPP::BufferedTransformation& bt) {
    CryptoPP::FileSource file(filename.c_str(), true /*pumpAll*/);

    file.TransferTo(bt);
    bt.MessageEnd();
}

void LoadPrivateKey(const std::string& filename, CryptoPP::RSA::PrivateKey& key) {
    CryptoPP::ByteQueue queue;
    Load(filename, queue);

    key.Load(queue);
}

void LoadPublicKey(const std::string& filename, CryptoPP::RSA::PublicKey& key) {
    CryptoPP::ByteQueue queue;
    Load(filename, queue);

    key.Load(queue);
}

std::string encrypt(std::string s, const CryptoPP::RSA::PrivateKey & key) {
    CryptoPP::AutoSeededRandomPool rng;
    // Signing
    CryptoPP::RSASS<CryptoPP::PSSR, CryptoPP::SHA256>::Signer signer( key);
    ////////////////////////////////////////////////
    // Sign and Encode
    CryptoPP::SecByteBlock signature(signer.MaxSignatureLength(str_to_block(s).size()));

    size_t signatureLen = signer.SignMessageWithRecovery(rng, str_to_block(s),
        str_to_block(s).size(), NULL, 0, signature);

    // Resize now we know the true size of the signature
    signature.resize(signatureLen);
    return block_to_str(signature);
}

std::string decrypt(std::string s, const CryptoPP::RSA::PublicKey & key) {
    CryptoPP::AutoSeededRandomPool rng;
    // Verifier object
    CryptoPP::RSASS<CryptoPP::PSSR, CryptoPP::SHA256>::Verifier verifier(key);
    ////////////////////////////////////////////////
    // Verify and Recover
    CryptoPP::SecByteBlock recovered(
        verifier.MaxRecoverableLengthFromSignatureLength(str_to_block(s).size())
    );

    CryptoPP::DecodingResult result = verifier.RecoverMessage(recovered, NULL,
        0, str_to_block(s), str_to_block(s).size());

    if (!result.isValidCoding) {
        std::cerr << "Invalid Signature\n";
    }

    ////////////////////////////////////////////////
    // Use recovered message
    //  MaxSignatureLength is likely larger than messageLength
    recovered.resize(result.messageLength);
    return block_to_str(recovered);
}
