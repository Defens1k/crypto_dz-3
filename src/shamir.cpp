#include "shamir.h"

std::vector<std::string> SecretShare(size_t threshold, size_t nShares, std::string secret) {
    CryptoPP::AutoSeededRandomPool rng; //for random
    CryptoPP::ChannelSwitch *channelSwitch;  //switch split output to n string
    CryptoPP::StringSource source(secret, false, new CryptoPP::SecretSharing(rng, threshold, nShares, channelSwitch = new CryptoPP::ChannelSwitch));

    std::vector<std::string> out_str(nShares);  //n result string
    CryptoPP::vector_member_ptrs<CryptoPP::StringSink> stringsinks(nShares);  // algorithm output >> result string
    std::string channel; // number of shard in string
    for (size_t i = 0; i < nShares; i++) {  // split output of SecretSharing algorithm to n string
        stringsinks[i].reset(new CryptoPP::StringSink(out_str[i]));
        channel = CryptoPP::WordToString<CryptoPP::word32>(i); // number of chunnel(pos x of point).  
        stringsinks[i]->Put((CryptoPP::byte *)channel.data(), sizeof(CryptoPP::word32)); //it need to work CryptoPP::SecretRecovery
        channelSwitch->AddRoute(channel, *stringsinks[i], CryptoPP::DEFAULT_CHANNEL);
    }
    source.PumpAll();  // pump input throught alghorithm
    return out_str;
}

std::string SecretRecover(size_t threshold, std::vector<std::string> input_str) {
    std::string result;
    CryptoPP::SecretRecovery recovery(threshold, new CryptoPP::StringSink(result)); 
    CryptoPP::vector_member_ptrs<CryptoPP::StringSource> stringSources(threshold); 
    CryptoPP::SecByteBlock channel(sizeof(CryptoPP::word32)); //number of channel (x of point )
    for (size_t i = 0; i < threshold; i++) {
        stringSources[i].reset(new CryptoPP::StringSource(input_str[i], false));  // covert string so algo sources
        stringSources[i]->Pump(sizeof(CryptoPP::word32));  
        stringSources[i]->Get(channel, sizeof(CryptoPP::word32));  //first 4 bytes is a number of chunnel
        stringSources[i]->Attach(new CryptoPP::ChannelSwitch(recovery, std::string((char *)channel.begin(), sizeof(CryptoPP::word32))));
    }
    for (size_t i = 0; i < threshold; i++) {
        stringSources[i]->PumpAll();  // pump str[i] throught alghorithm
    }
    return result;
}
