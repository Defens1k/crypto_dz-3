#include "hex_functions.h"
std::string unhex_str(std::string input) {
    std::string result;
       
    CryptoPP::StringSource ss(input, true,
        new CryptoPP::HexDecoder(
            new CryptoPP::StringSink(result)
        ) // HexDecoder
    ); // StringSource
    return result;
}

std::string hex_str(std::string input) {
    std::string result;

    CryptoPP::StringSource ss(input, true,
        new CryptoPP::HexEncoder(
            new CryptoPP::StringSink(result)
        ) // HexEncoder
    ); // StringSource

    return result;
}

std::vector<std::string> hex(std::vector<std::string> input) {
    std::vector<std::string> result;
    for (auto str : input) {
        result.push_back(hex_str(str));
    }
    return result;
}

std::vector<std::string> encode_hex(std::vector<std::string> input) {
    std::vector<std::string> result;
    for (auto str : input) {
        result.push_back(unhex_str(str));
    }
    return result;
}
