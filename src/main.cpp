#include <iostream>
#include <string>
#include "hex_functions.h"
#include "shamir.h"


int main(int argc, const char ** argv) {
    if (argc < 2) {
        std::cerr << "Missing arguments" << std::endl;
        return -1;
    }
    if (std::string("split") == std::string(argv[1])) {
        std::string secret;
        std::cin >> secret;
        size_t t = 0, n = 0;
        std::cin >> n >> t;
        std::vector<std::string> shared_secret = SecretShare(t, n, secret);
        std::vector<std::string> shared_secret_hexed = hex(shared_secret);
        for (auto s : shared_secret_hexed) {
            std::cout << s << std::endl;
        }
    } else if(std::string("recover") == std::string(argv[1])) {
        std::vector<std::string> shared_secred_hexed;
        std::string input;
        while(std::cin >> input) {
            shared_secred_hexed.push_back(input);
        }
        std::vector<std::string> shared_secret = encode_hex(shared_secred_hexed);

        std::string secret = SecretRecover(shared_secret.size(), shared_secret);
        std::cout << secret << std::endl;
    } else {
        std::cerr << "argument must be \"split\" or \"recover\"" << std::endl;
    }
}


